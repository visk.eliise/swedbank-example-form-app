jQuery(function(){
    const beginButton = $('.app__form-begin');
    const nextButton = $('.app__form-next');
    const prevButton = $('.app__form-prev');
    const submitButton = $('.app__form-submit');

    beginButton.on('click', (event) => {
        const currentSection = $(event.currentTarget).parents('section');
        
        currentSection.next().removeClass('is-hidden');
        currentSection.addClass('is-hidden');
    })

    nextButton.on('click', (event) => {
        const conclusion = $('.app__form-conclusion');
        const currentSection = $(event.currentTarget).parents('section');
        const textfield = $(currentSection).find('input[type=text]');
        const textarea = $(currentSection).find('textarea');
        const radio = $(currentSection).find('input[type=radio]');
        const check = $(currentSection).find('input[type=checkbox]');
        const select = $(currentSection).find('select');
        let isValidated = true;
        
        if (textfield.length != 0) {
            if (textfield.val() === '') {
                isValidated = false;
                textfield.parents('.textfield').addClass('is-invalid');
                currentSection.find('.app__form-error').removeClass('is-hidden');
            } else {
                isValidated = true;
                conclusion.find('[data-fill=name]').text(textfield.val());
    
                textfield.parents('.textfield').removeClass('is-invalid');
                currentSection.find('.app__form-error').addClass('is-hidden');
            }
        }

        if (textarea.length != 0) {
            if (textarea.val() === '') {
                isValidated = false;
                textarea.parents('.textfield').addClass('is-invalid');
                currentSection.find('.app__form-error').removeClass('is-hidden');
            } else {
                isValidated = true;
                conclusion.find('[data-fill=explanation]').text(textarea.val());
    
                textarea.parents('.textfield').removeClass('is-invalid');
                currentSection.find('.app__form-error').addClass('is-hidden');
            }
        }

        if (radio.length != 0) {
            const checkedRadio =  $(currentSection).find('input[type=radio]:checked');
            if (checkedRadio.length === 0) {
                isValidated = false;
                radio.parents('radio').addClass('is-invalid');
                currentSection.find('.app__form-error').removeClass('is-hidden');
            } else if (checkedRadio.length != 0) {
                isValidated = true;
                conclusion.find('[data-fill=vehicle-state]').text(radio.val());
    
                radio.parents('radio').removeClass('is-invalid');
                currentSection.find('.app__form-error').addClass('is-hidden');
            }
        }

        if (check.length != 0) {
            const checkedCheck = $(currentSection).find('input[type=checkbox]:checked');

            if (checkedCheck.length === 0) {
                isValidated = false;
                checkedCheck.parents('check').addClass('is-invalid');
                currentSection.find('.app__form-error').removeClass('is-hidden');
            } else if (checkedCheck.length != 0) {
                isValidated = true;
                
                const selectedItems = [];
                $(checkedCheck).each((index, element) => {
                    selectedItems.push($(element).val());
                })
                conclusion.find('[data-fill=vehicle-properties]').text(selectedItems);
    
                checkedCheck.parents('check').removeClass('is-invalid');
                currentSection.find('.app__form-error').addClass('is-hidden');
            }
        }

        if (select.length != 0) {
            if (select.val() === null) {
                isValidated = false;
            } else {
                isValidated = true;
                conclusion.find('[data-fill=manufacturer]').text(select.val());
            }
        }

        if (isValidated) {
            currentSection.next().removeClass('is-hidden');
            currentSection.addClass('is-hidden');
        }
    })

    prevButton.on('click', (event) => {
        const currentSection = $(event.currentTarget).parents('section');
        currentSection.prev().removeClass('is-hidden');
        currentSection.addClass('is-hidden');
    })

    submitButton.on('click', (event) => {
        event.preventDefault();
        $('.app__form').addClass('is-hidden');
        $('.app__gratitude').removeClass('is-hidden');
    })

    $('.textfield__tooltip').hide();

    $('.textfield__input').on('mousemove', (e) => {
            $('.textfield__tooltip').offset({
                left:  e.pageX + 20,
                top:   e.pageY
            });
        });

    $('.textfield__input').on('mouseenter', () => {
        $('.textfield__tooltip').show();
    });

    $('.textfield__input').on('mouseleave', () => {
        $('.textfield__tooltip').hide();
    });

    $('form input').on('keypress', function(e) {
        return e.which !== 13;
    });
})